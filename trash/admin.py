from django.contrib import admin
from models import Trash, Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    pass


@admin.register(Trash)
class TrashAdmin(admin.ModelAdmin):
    pass
