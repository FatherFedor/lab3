from django.views import generic
from django.http import HttpResponseRedirect

from django.db.models.functions import Trunc
import models
from forms import TaskForm
from helper import FileDescrip


import os
import json


class FileManager(generic.ListView):
    template_name = 'file_manager.html'
    context_object_name = 'contents'
    # paginate_by = 25

    def get_previos_path(self):
        return os.path.abspath(os.path.join(self.get_current_path(),
                               os.pardir))

    def get_current_path(self):
        path = self.request.GET.get('path')
        if not path:
            path = '~/'
        path = os.path.expanduser(path)
        return path

    def get_context_data(self, **kwargs):
        context = super(FileManager, self).get_context_data(**kwargs)
        context['previos_dir'] = self.get_previos_path()

        return context

    def get_queryset(self):
        file_list = []
        path = self.get_current_path()
        for name in os.listdir(path):
            file_list.append(FileDescrip(os.path.join(path, name)))
        return file_list


class TrashList(generic.ListView):
    template_name = 'trash_list.html'
    context_object_name = 'trashs'
    model = models.Trash


class TrashView(generic.ListView):
    template_name = 'trash_detail.html'
    model = models.Trash
    context_object_name = 'contents'

    def post(self, request, *args, **kwargs):
        t = models.Trash.objects.filter(trash_dir=self.request.POST.get('path')).first()
        t.clean()
        return HttpResponseRedirect('/trash/')

    def get_current_path(self):
        path = self.request.GET.get('path')
        if not path:
            path = '~/'
        path = os.path.expanduser(path)
        return path

    def get_context_data(self, **kwargs):
        context = super(TrashView, self).get_context_data(**kwargs)
        context['path'] = self.get_current_path()
        return context

    def get_queryset(self):
        file_list = []
        path = self.get_current_path()
        for name in os.listdir(path):
            file_list.append(FileDescrip(os.path.join(path, name)))
        return file_list


class TaskView(generic.ListView):
    template_name = 'task_list.html'
    context_object_name = 'tasks'
    model = models.Task
    paginate_by = 30

    def post(self, request, *args, **kwargs):
        t = models.Trash.objects.filter(name_trash=self.request.POST.get('trashes')).first()
        print self.request.POST
        task = models.Task.objects.create(action=self.request.POST.get('action'),
                                          status='inprogress',
                                          files=json.dumps(self.request.POST.getlist('selected_files')),
                                          trash=t,
                                          regex=self.request.POST.get('regex_str'),
                                          policy=self.request.POST.get('policy'))
        task.execute()

        return HttpResponseRedirect('/task/')

    def get_queryset(self):
        return models.Task.objects.all().order_by('-date')


class TaskCreate(generic.FormView):
    template_name = 'task_creating.html'
    form_class = TaskForm

    def get_context_data(self, **kwargs):
        context = super(TaskCreate, self).get_context_data(**kwargs)
        print self.request.POST
        context['action'] = self.request.POST.get('action')
        context['files'] = self.request.POST.getlist('selected_files')
        return context
