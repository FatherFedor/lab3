from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'manager/$', views.FileManager.as_view(), name="filemanager"),
    url(r'trash/$', views.TrashList.as_view(), name='trash'),
    url(r'trash/detail/$', views.TrashView.as_view(), name='detail_trash'),
    url(r'task/$', views.TaskView.as_view(), name='task'),
    url(r'task/create/$', views.TaskCreate.as_view(), name='task_create')
]
