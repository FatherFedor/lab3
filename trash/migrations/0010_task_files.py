# -*- coding: utf-8 -*-
# Generated by Django 1.10.1 on 2017-06-30 14:18
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('trash', '0009_remove_task_files'),
    ]

    operations = [
        migrations.AddField(
            model_name='task',
            name='files',
            field=models.CharField(default='replace', max_length=500),
            preserve_default=False,
        ),
    ]
