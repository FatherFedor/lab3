from __future__ import unicode_literals

from django.db import models

from django.utils import timezone

from ssrc.trash import Trash as MyTrash
from functools import partial
import pathos.multiprocessing as multiprocessing
import json


class Trash(models.Model):

    trash_dir = models.CharField(max_length=50)
    info_dir = models.CharField(max_length=50)
    name_trash = models.CharField(max_length=50)

    def __str__(self):
        return self.name_trash

    def clean(self):
        t = MyTrash()
        t.clear_all_trash(self.trash_dir, self.info_dir)


class Task(models.Model):
    ACTION_CHOICE = (
        ('delete', 'delete'),
        ('restore', 'restore'),
    )
    STATUS = (
        ('complete', 'complete'),
        ('inprogress', 'inprogress'),
        ('error', 'error'),
    )
    date = models.DateTimeField(default=timezone.now)
    files = models.CharField(max_length=500)
    action = models.CharField(max_length=50, choices=ACTION_CHOICE)
    status = models.CharField(max_length=50, choices=STATUS)
    policy = models.CharField(max_length=50)
    trash = models.ForeignKey(Trash)
    regex = models.CharField(max_length=50, blank=True, null=True)

    def get_files(self):
        return json.loads(self.files)

    def execute(self):
        try:
            pool_size = multiprocessing.cpu_count() * 2
            pool = multiprocessing.Pool(processes=pool_size)

            files = self.get_files()

            if self.action == 'delete':
                t = MyTrash(policy_delete=self.policy)
                if self.regex:
                    delete_func = partial(t.regex_delete,
                                          regex=self.regex,
                                          path_directory_of_trash=self.trash.trash_dir,
                                          path_info=self.trash.info_dir,
                                          max_size=500000)
                else:
                    delete_func = partial(t.delete,
                                          path_directory_of_trash=self.trash.trash_dir,
                                          path_info=self.trash.info_dir,
                                          max_size=500000)

                pool.map(delete_func, files)
            if self.action == 'restore':
                t = MyTrash(policy_restore=self.policy)
                restore_func = partial(t.restore,
                                       path_directory_of_trash=self.trash.trash_dir,
                                       path_info=self.trash.info_dir,
                                       max_size=500000)

                pool.map(restore_func, files)
            pool.close()
            pool.join()
            self.status = 'complete'
            self.save()
        except Exception as e:
            print e
            self.status = 'error'
            self.save()
