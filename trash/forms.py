from django import forms
from models import Trash

POLICY_CHOICES = (('gen_uniq', 'gen_uniq'),
                  ('replace', 'replace'),
                  ('cancel', 'cancel'))


class TaskForm(forms.Form):
    trashes = forms.ChoiceField([(trash.name_trash, trash.trash_dir) for trash in Trash.objects.all()])
    policy = forms.ChoiceField(POLICY_CHOICES)
